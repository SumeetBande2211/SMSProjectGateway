function getCategoryNamesForSelectedBusiness(){
	$('#loading').html('<img src="./resources/img/ajax-loader.gif">');
	debugger;
		var business = $('#business').val();
		$.ajax({
			url : 'addcategory/getCategoryNamesForSelectedBusiness',
			method : 'post',
			ContentType :'json',
			data : {
				business : business
			},

			success : function(response) {
				var options = '';
				if (response != null) {
					$(response).each(function(index, value) {
						options = options + '<option>' + value + '</option>';
					});
					$('#parent').html(options);
				}
				$('#loading').html('');
			}
		});
	}