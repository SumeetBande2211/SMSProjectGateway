<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>EZEEORDER</title>


</script>
<!-- //for-mobile-apps -->
<link href="<c:url value="/resources/css/bootstrap.css"/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"
	type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="<c:url value="/resources/css/font-awesome.css"/>"
	rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js -->
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<title>Registration Form</title>

<!-- Bootstrap -->

<link href="css/dcalendar.picker.css" rel="stylesheet">
<style type="text/css">
#deceased {
	background-color: #FFF3F5;
	padding-top: 10px;
	margin-bottom: 10px;
}

.remove_field {
	float: right;
	cursor: pointer;
	position: absolute;
}

.remove_field:hover {
	text-decoration: none;
}
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.12.4.js"></script>
<script src="js/dcalendar.picker.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->



<!-- //js -->
<link
	href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script src="<c:url value="/resources/js/passtest.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/easing.js"/>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>

<body>
	<!-- header -->
	


	<!-- //header -->
	<!-- navigation -->
	<!-- <div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default"> -->
			<!-- 	<!-- Brand and toggle get grouped for better mobile display -->
			
				<!-- <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.html" class="act">Home</a></li>
						Mega Menu


						<li><a href="homesearch.html">Search</a></li>

						<li><a href="aboutus.html">About Us</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div> -->

	<!-- //navigation -->
	<!-- breadcrumbs -->
	
	<!-- //breadcrumbs -->
	<!-- register -->
	<%-- <p style="color: red;">
					<b>${msg}</b>
				</p> --%>
	<div class="panel panel-primary" style="margin: 50px;">
	
		<div class="panel-heading "></div>
		<div class="panel-body">
			<form:form action="${pageContext.request.contextPath}/saveUser"
				commandName="adduser" method="post"> 
				
				<div class="col-md-12 col-sm-12" id="deceased">
					<div class="form-group col-md-6 col-sm-6">
						<label for="userName"> Name* </label>
						<form:errors path="userName" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="userName"
							id="userName" placeholder="" required=" " />
					</div>
					

					<div class="form-group col-md-6 col-sm-6">
						<label for="userMobile">Mobile no*</label>
						<form:errors path="userMobile" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="userMobile"
							id="userMobile" placeholder="" required=" " />
					</div>

					<%-- <div class="form-group col-md-6 col-sm-6">
						<label for="landline">LandLine no*</label>
						<form:errors path="landline" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="landline"
							id="landline" placeholder="" required=" " />
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label for="chooseRole">Choose Role*</label>

						<form:errors path="chooseRole" cssStyle="color:#ff0000" />
						<form:select class="form-control input-sm" path="chooseRole"
							id="userType">
							<form:option value="0" label="--Select Role--" />
							<form:option value="1">Personal</form:option>
							<form:option value="2">Retailer</form:option>
							<form:option value="3">Dealer</form:option>
							<form:option value="4">Company</form:option>
							<form:option value="5">MR</form:option>
							<form:option value="6">Salesman</form:option>
							<form:option value="7">Delivery Boy</form:option>
						</form:select>
					</div>
					<div class="form-group col-md-6 col-sm-6" required=" ">
						<label for="userType">Choose User Type*</label>
						<form:errors path="userType" cssStyle="color:#ff0000" />
						<form:select class="form-control input-sm" path="userType"
							id="userType">
							<form:option value="0" label="--Select User--" />
							<form:option value="1">FreeUser</form:option>
							<form:option value="2">ProUser</form:option>
							<form:option value="3">ProPlusUser</form:option>
						</form:select>
					</div>

					<div class="form-group col-md-6 col-sm-6">
						<label for="firmName">Firm Name*</label>
						<form:errors path="firmName" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="firmName"
							id="firmName" placeholder="" required=" " />
					</div>

					<div class="form-group col-md-6 col-sm-6">
						<label for="websiteName">WebSite Name*</label>
						<form:errors path="websiteName" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="websiteName"
							id="websiteName" placeholder="" required=" " />
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label for="stateName">State *</label>
						<form:errors path="stateName" cssStyle="color:#ff0000" />
						<form:select class="form-control input-sm" id="state" name="state"
							onblur="getCitiesForSelectedState()" path="stateName">
							<form:option value="-1" label="--Select State--" />
							<c:forEach items="${stateList}" var="stateList">
								<form:option value="${stateList.stateName}">${stateList.stateName}</form:option>>
							</c:forEach>
						</form:select>

					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label for="cityName">City*</label>
						<form:errors path="districtName" cssStyle="color:#ff0000" />
						<form:select class="form-control input-sm" id="city" name="city"
							onblur="getTalukaForSelectedDistrict()" path="districtName">
							<form:option value="Select District" label="--Select District--" />
						</form:select>
						<div id="loading"></div>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label for="taluka">Choose Taluka*</label>
						<form:errors path="taluka" cssStyle="color:#ff0000" />
						<form:select class="form-control input-sm" id="taluka" name="taluka" path="taluka" >
							<form:option value="0" label="--Select Taluka--" />
						</form:select>
						<div id="loading"></div>
					</div>

					<div class="form-group col-md-6 col-sm-6">
						<label for="city">City/Area*</label>
						<form:errors path="city" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="city"
							placeholder="" required=" " />
					</div>

					<div class="form-group col-md-6 col-sm-6">
						<label for="address">Address*</label>
						<form:errors path="address" cssStyle="color:#ff0000" />
						<form:textarea class="form-control input-sm" path="address"
							id="address" rows="3" />
						
					</div>

					<div class="form-group col-md-6 col-sm-6">
						<label for="pinCode">Pincode</label>
						<form:errors path="pinCode" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="pinCode"
							id="pinCode" placeholder="" required=" " />
					</div>

				</div>
				<div class="col-md-12 col-sm-12" id="deceased">
					<div class="form-group col-md-4 col-sm-4">
						<label for="email">Email*</label>
						<form:errors path="email" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="email" id="email"
							placeholder="" required=" " />
					</div>
					<div class="form-group col-md-4 col-sm-4">
						<label for="password">Password*</label>
						<form:errors path="password" cssStyle="color:#ff0000" />
						<form:password path="password" class="form-control input-sm"
							id="pass1" placeholder="" required=" " />
					</div>
					<div class="form-group col-md-4 col-sm-4">
						<label for="cnfPassword">Confirm Password*</label>
						<form:errors path="cnfPassword" cssStyle="color:#ff0000" />
						<form:password path="cnfPassword" class="form-control input-sm"
							id="pass2" onkeyup="checkPass(); return false;" placeholder=""
							required=" " />
						<span id="confirmMessage" class="confirmMessage"></span>
					</div>

 --%>
				</div>

				<div class="panel-footer panel-info">
					<input type="submit" class="btn btn-primary" value="SUBMIT" />
				</div>
			</form:form> 
		</div>

	</div>
	<!-- //register -->
	<!-- //footer -->
	
	<!-- //main slider-banner -->
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	
</body>
</html>
