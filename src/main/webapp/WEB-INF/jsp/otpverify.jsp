<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>EZEEORDER</title>


</script>
<!-- //for-mobile-apps -->
<link href="<c:url value="/resources/css/bootstrap.css"/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"
	type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="<c:url value="/resources/css/font-awesome.css"/>"
	rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js -->
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<title>Registration Form</title>

<!-- Bootstrap -->

<link href="css/dcalendar.picker.css" rel="stylesheet">
<style type="text/css">
#deceased {
	background-color: #FFF3F5;
	padding-top: 10px;
	margin-bottom: 10px;
}

.remove_field {
	float: right;
	cursor: pointer;
	position: absolute;
}

.remove_field:hover {
	text-decoration: none;
}
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.12.4.js"></script>
<script src="js/dcalendar.picker.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->



<!-- //js -->
<link
	href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script src="<c:url value="/resources/js/passtest.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/easing.js"/>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>

<body>

<form:form action="${pageContext.request.contextPath}/verify"
				commandName="adduser" method="post"> 
				
				<h3>OTP send to registered mobile number</h3>
				<h2>${msg}</h2>
				 <%-- <div class="col-md-12 col-sm-12" id="deceased">
					<div class="form-group col-md-6 col-sm-6">
						<label for="userMobile">Phone * </label>
						<form:errors path="userMobile" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="userMobile"
							id="userMobile" placeholder="" required=" " />
					</div>
					</div>   --%>
				<div class="col-md-12 col-sm-12" id="deceased">
					<div class="form-group col-md-6 col-sm-6">
						<label for="otp"> Verify OTP* </label>
						<form:errors path="otp" cssStyle="color:#ff0000" />
						<form:input class="form-control input-sm" path="otp"
							id="otp" placeholder="" required=" " />
					</div>
					</div>
					
					<div class="panel-footer panel-info">
					<input type="submit" class="btn btn-primary" value="SUBMIT" />
				</div>
					</form:form>
</body>
</html>