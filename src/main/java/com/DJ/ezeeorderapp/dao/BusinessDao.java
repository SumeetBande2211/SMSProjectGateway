package com.DJ.ezeeorderapp.dao;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.sql.DataSource;

import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import com.DJ.ezeeorderapp.controller.OtpMessageTest;
import com.DJ.ezeeorderapp.controller.RegistrationController;
import com.DJ.ezeeorderapp.model.AddBusiness;
import com.DJ.ezeeorderapp.model.AppRegistration;
import com.DJ.ezeeorderapp.model.ChartData;
import com.DJ.ezeeorderapp.model.ProductMaster;
import com.DJ.ezeeorderapp.model.PutOrder;
import com.DJ.ezeeorderapp.model.RegisterToMyCT;
import com.DJ.ezeeorderapp.model.SubUser;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Repository
public class BusinessDao {

	public String otp = "";
	private String password = "";
	public String u_otp = "";
	public String status0 = "0";
	public String status = "1";
	public String statusone = "1";
	public String uMobile = "";
	public String dealerMobile = "";
	public String refrenceMobile = "";
	public String oauth = "";
	public String space = "";
	boolean passwordExist = false;
	public String dealerId = "";
	public String retailerId = "";
	// public String dealerMobile="";
	String retailerMob = "";
	private JdbcTemplate jdbcTemplate;
	public Map umobile, u_imei, update_data;

	private SimpleJdbcCall simpleJdbcCall;
	private DataSource dataSource;
	private SimpleJdbcCall simpleJdbcCall1;
	private SimpleJdbcCall simpleJdbcCall2;
	private SimpleJdbcCall simpleJdbcCall3;
	private SimpleJdbcCall simpleJdbcCall4;
	OtpMessageTest messageTest = new OtpMessageTest();
	String OTPmsg = "Your-secure-Otp-is:";

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.simpleJdbcCall = new SimpleJdbcCall(this.dataSource).withProcedureName("otpverify");
		this.simpleJdbcCall1 = new SimpleJdbcCall(this.dataSource).withProcedureName("appregistration");
		this.simpleJdbcCall2 = new SimpleJdbcCall(this.dataSource).withProcedureName("RegisterToMyct");
		this.simpleJdbcCall3 = new SimpleJdbcCall(this.dataSource).withProcedureName("STORE_OTP");
		this.simpleJdbcCall4 = new SimpleJdbcCall(this.dataSource).withProcedureName("downloadOrderList");

	}

	public String returnOtp() {
		Integer value = (int) (Math.random() * 9999) + 1001;
		otp = value.toString();
		return otp;
	}


	public List<AppRegistration> getAllUser() {
		String sql = "SELECT  * FROM otp";
		List<AppRegistration> listUsers = jdbcTemplate.query(sql, new RowMapper<AppRegistration>() {

			@Override
			public AppRegistration mapRow(ResultSet rs, int rowNum) throws SQLException {
				AppRegistration user = new AppRegistration();
				user.setUserMobile(rs.getString("userMobile"));
				// uMobile = user.getUserMobile();
				user.setOtp(rs.getString("otp"));
				u_otp = rs.getString("otp");
				return user;
			}

		});

		return listUsers;
	}

	/**********
	 * OTP implementation
	 * 
	 * @throws Exception
	 **********/

	@SuppressWarnings({ "deprecation" })
	public AppRegistration registerApp(AppRegistration appRegistration) throws Exception {
		/****** Verify based on condition ******/

		/********* Comparing Dummy mobile no and status *******/
		List<AppRegistration> registration1 = null;
		try {
			registration1 = jdbcTemplate.query("SELECT * FROM otp",
					new BeanPropertyRowMapper<AppRegistration>(AppRegistration.class));
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		if (registration1.size() > 0) {

			try {
				String sql0 = "SELECT userMobile from otp where userMobile=? ";
				uMobile = jdbcTemplate.queryForObject(sql0, String.class, appRegistration.getUserMobile());

			} catch (EmptyResultDataAccessException e) {
				System.out.println(e);
				Integer value1 = (int) (Math.random() * 9999) + 1001;
				// otp = "" + value;
				otp = value1.toString();
				OtpMessageTest test1 = new OtpMessageTest();
				String msg1 = "Your-secure-otp-is:";
				// String msg1="from:";
				status = "1";

				SqlParameterSource parameter = new MapSqlParameterSource().addValue("u_action", "INSERT")
						.addValue("u_Name", appRegistration.getUserName())
						.addValue("u_Mobile", appRegistration.getUserMobile())
						.addValue("u_imei", appRegistration.getImei()).addValue("u_otp", otp)
						.addValue("u_status", status);
				simpleJdbcCall1.execute(parameter);

				SqlParameterSource otpParam = new MapSqlParameterSource().addValue("u_action", "INSERT1")
						.addValue("u_Name", "").addValue("u_Mobile", appRegistration.getUserMobile())
						.addValue("u_imei", appRegistration.getImei()).addValue("u_otp", otp)
						.addValue("u_status", status);
				simpleJdbcCall1.execute(otpParam);
				test1.sendSms(appRegistration.getUserMobile(), msg1, otp, space);
				System.exit(1);
			}

			/***** If mobile no is same and imei is same *****/

			if (uMobile.equals(appRegistration.getUserMobile())) {
				String sql2 = "SELECT imei from otp where userMobile=?";
				String u_imei1 = jdbcTemplate.queryForObject(sql2, String.class, appRegistration.getUserMobile());
				/*
				 * SqlParameterSource imeiParam=new MapSqlParameterSource()
				 * .addValue("u_action", "SELECT_IMEI") .addValue("u_Name", "")
				 * .addValue("u_imei", appRegistration.getImei())
				 * .addValue("u_otp", "") .addValue("u_status", "")
				 * .addValue("u_Mobile", appRegistration.getUserMobile());
				 * u_imei=simpleJdbcCall1.execute(imeiParam);
				 */

				if (u_imei1.equals(appRegistration.getImei()) && uMobile.equals(appRegistration.getUserMobile())) {
					/*
					 * Integer count5 = jdbcTemplate.
					 * update("UPDATE app_registration set userName=? where userMobile=?"
					 * , new Object[] { appRegistration.getUserName(),
					 * appRegistration.getUserMobile() });
					 */
					// test.sendSms(appRegistration.getUserMobile(), msg, otp);

					SqlParameterSource imeiParam = new MapSqlParameterSource()
							.addValue("u_action", "UPDATE_REGISTRATION")
							.addValue("u_Name", appRegistration.getUserName()).addValue("u_imei", "")
							.addValue("u_otp", "").addValue("u_status", "")
							.addValue("u_Mobile", appRegistration.getUserMobile());
					update_data = simpleJdbcCall1.execute(imeiParam);

					return appRegistration;

					/********
					 * if mobile no is same and imei is different
					 ********/

				} else {
					Integer value = (int) (Math.random() * 9999) + 1001;
					// otp = "" + value;
					otp = value.toString();
					OtpMessageTest test = new OtpMessageTest();
					String msg = "Your-secure-otp-is:";

					SqlParameterSource imeiParam = new MapSqlParameterSource().addValue("u_action", "UPDATE_OTP")
							.addValue("u_Name", "").addValue("u_imei", appRegistration.getImei()).addValue("u_otp", otp)
							.addValue("u_status", status).addValue("u_Mobile", appRegistration.getUserMobile());
					update_data = simpleJdbcCall1.execute(imeiParam);

					test.sendSms(appRegistration.getUserMobile(), msg, otp, space);
					return appRegistration;
				}
			}
		}
		/******** For new User registration *********/
		else {
			Integer value = (int) (Math.random() * 9999) + 1001;
			// otp = "" + value;
			otp = value.toString();
			OtpMessageTest test = new OtpMessageTest();
			String msg = "Your-secure-otp-is:";
			// String msg1="from:";
			status = "1";

			SqlParameterSource parameter = new MapSqlParameterSource().addValue("u_action", "INSERT")
					.addValue("u_Name", appRegistration.getUserName())
					.addValue("u_Mobile", appRegistration.getUserMobile()).addValue("u_imei", appRegistration.getImei())
					.addValue("u_otp", otp).addValue("u_status", status);
			simpleJdbcCall1.execute(parameter);
			test.sendSms(appRegistration.getUserMobile(), msg, otp, space);

			SqlParameterSource otpParam = new MapSqlParameterSource().addValue("u_action", "INSERT1")
					.addValue("u_Name", "").addValue("u_Mobile", appRegistration.getUserMobile())
					.addValue("u_imei", appRegistration.getImei()).addValue("u_otp", otp).addValue("u_status", status);
			simpleJdbcCall1.execute(otpParam);

		}

		return appRegistration;

	}

	/******** OTP verification **********/

	/**** OTP verification *****/

	@SuppressWarnings({ "deprecation", "unchecked" })
	public boolean OTPVerify(AppRegistration appRegistration) {
		// AppRegistration appRegistration=new AppRegistration();
		boolean otpexist = false;
		int i = Integer.parseInt(appRegistration.getOtp());
		int count = jdbcTemplate.queryForInt("SELECT otp from otp where userMobile=? ",
				appRegistration.getUserMobile());
		if (count == i) {
			count = jdbcTemplate.update("UPDATE otp SET otp ='0',status='0' where  userMobile=? ",
					new Object[] { appRegistration.getUserMobile() });
			if (count == 0) {
			}
			otpexist = true;
		}
		return otpexist;
	}

	/************************************
	 * REGISTER TO MY CITY
	 *******************************************/

	public String register(RegisterToMyCT register) {

		List<RegisterToMyCT> registerToMyCT = null;
		String SERVERID = "";
		String enteredmobile = register.getAppMobileNo();
		String enteredImei = register.getStrDevId();
		Integer enteredRoleId = register.getRoleId();

		try {

			registerToMyCT = jdbcTemplate.query("SELECT * from RegisterToMyCT LIMIT 1",
					new BeanPropertyRowMapper<RegisterToMyCT>(RegisterToMyCT.class));

		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}

		if (registerToMyCT.size() > 0) {
			try {
				uMobile = jdbcTemplate.queryForObject("SELECT appMobileNo from RegisterToMyCT where appMobileNo=?",
						String.class, register.getAppMobileNo());
			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
			}

			// new entry

			if (!uMobile.equals(enteredmobile)) {

				Integer value = (int) (Math.random() * 9999) + 1001;
				otp = value.toString();

				Integer value1 = (int) (Math.random() * 9999) + 1001;
				password = value1.toString();

				oauth = UUID.randomUUID().toString();
				try {
					int count = jdbcTemplate.update(
							"Insert into RegisterToMyCT(appMobileNo,refMobileNo,strDevId,userSimSerialNo,"
									+ "keyword,firstName,lastName,firmName,address,schoolCode,"
									+ "emailId,roleId,pincode,passcode,latitude,longitude,state,"
									+ "district,taluka,userType,dealerMobileno)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
							register.getAppMobileNo(), register.getRefMobileNo(), register.getStrDevId(),
							register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
							register.getLastName(), register.getFirmName(), register.getAddress(),
							register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
							register.getPincode(), register.getPasscode(), register.getLatitude(),
							register.getLongitude(), register.getState(), register.getDistrict(), register.getTaluka(),
							register.getUserType(), register.getDealerMobileno());

				} catch (Exception e) {
					// TODO: handle exception
				}

				try {

					SERVERID = jdbcTemplate.queryForObject("SELECT max(regId) from RegisterToMyCT", String.class);

				} catch (EmptyResultDataAccessException e) {
					// TODO: handle exception
				}

				try {
					int count = jdbcTemplate.update("INSERT into StoreOTP(appMobileNo,otp)values(?,?)",
							register.getAppMobileNo(), otp);

				} catch (Exception e) {
					// TODO: handle exception
				}

				try {

					int count = jdbcTemplate.update(
							"INSERT into tbl_password(appMobileNo,userPassword,oauth)values(?,?,?)",
							register.getAppMobileNo(), password, oauth);

				} catch (Exception e) {
					// TODO: handle exception
				}

				try {

					String sql = "Select regId,appMobileNo,refMobileNo,strDevId,userSimSerialNo,keyword,firstName,lastName,"
							+ "firmName,address,schoolCode,emailId,roleId,pincode,passcode,latitude,longitude,state,"
							+ "district,taluka,userType,dealerMobileno from RegisterToMyCT where appMobileNo=? && regId=?";

					registerToMyCT = jdbcTemplate.query(sql, new Object[] { register.getAppMobileNo(), SERVERID },
							new BeanPropertyRowMapper<RegisterToMyCT>(RegisterToMyCT.class));
				} catch (EmptyResultDataAccessException e) {
					// TODO: handle exception
				}
				messageTest.sendSms(register.getAppMobileNo(), OTPmsg, otp, space);
				return "NewRegistration";
			}

			if (uMobile.equals(enteredmobile)) {
				String imei = "";
				String dbOTP = "";
				int roleId = 0;

				try {

					String sql1 = "SELECT strDevId from RegisterToMyCT where appMobileNo=?";
					imei = jdbcTemplate.queryForObject(sql1, String.class, register.getAppMobileNo());

					String sql2 = "SELECT otp from StoreOTP where appMobileNo=?";
					dbOTP = jdbcTemplate.queryForObject(sql2, String.class, register.getAppMobileNo());

					String sql3 = "SELECT roleId from RegisterToMyCT where appMobileNo=?";
					roleId = jdbcTemplate.queryForObject(sql3, Integer.class, register.getAppMobileNo());
				} catch (Exception e) {
					// TODO: handle exception
				}

				int finalRoleId = roleId;

				/*****
				 * If mobile no is same and imei is same but otp is not verified
				 *****/

				if (imei.equals(enteredImei) && uMobile.equals(enteredmobile) && !dbOTP.equals("1")
						&& finalRoleId == enteredRoleId) {

					try {

						int count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,"
										+ "passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=? where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getStrDevId(),
										register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
										register.getLastName(), register.getFirmName(), register.getAddress(),
										register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
										register.getPincode(), register.getPasscode(), register.getLatitude(),
										register.getLongitude(), register.getState(), register.getDistrict(),
										register.getTaluka(), register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

					} catch (Exception e) {
						// TODO: handle exception
					}
					messageTest.sendSms(register.getAppMobileNo(), OTPmsg, dbOTP, space);
					return "OTPNotVerified";
				}

				/*****
				 * If mobile no is same and imei is same but otp is not verified
				 * and role changed
				 *****/

				if (imei.equals(enteredImei) && uMobile.equals(enteredmobile) && !dbOTP.equals("1")
						&& finalRoleId != enteredRoleId) {
					/*
					 * String finalStatus = "0";
					 * messageTest.sendSms(register.getAppMobileNo(), OTPmsg,
					 * dbOTP, space);
					 * 
					 * try {
					 * 
					 * String sql =
					 * "SELECT otp from StoreOTP where appMobileNo=?";
					 * finalStatus = jdbcTemplate.queryForObject(sql,
					 * String.class, register.getAppMobileNo());
					 * 
					 * } catch (Exception e) { // TODO: handle exception }
					 * 
					 * if (finalStatus.equals("1")) {
					 */

					try {

						int count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=?  where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getStrDevId(),
										register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
										register.getLastName(), register.getFirmName(), register.getAddress(),
										register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
										register.getPincode(), register.getPasscode(), register.getLatitude(),
										register.getLongitude(), register.getState(), register.getDistrict(),
										register.getTaluka(), register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

					} catch (Exception e) {
						// TODO: handle exception
					}
					return "OTPNotVerifiedAndRoleChanged";
					/*
					 * } else { return "OTPNotVerifiedAndRoleChanged"; }
					 */

				}

				/*****
				 * If mobile no is same and imei is same and otp is verified
				 *****/

				if (imei.equals(enteredImei) && uMobile.equals(enteredmobile) && dbOTP.equals("1")
						&& finalRoleId == enteredRoleId) {
					try {

						int count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=?  where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getStrDevId(),
										register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
										register.getLastName(), register.getFirmName(), register.getAddress(),
										register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
										register.getPincode(), register.getPasscode(), register.getLatitude(),
										register.getLongitude(), register.getState(), register.getDistrict(),
										register.getTaluka(), register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

					} catch (Exception e) {
						// TODO: handle exception
					}
					return "OTPVerified";
				}

				/*****
				 * If mobile no is same and imei is same and otp is verified and
				 * role changed
				 *****/

				if (imei.equals(enteredImei) && uMobile.equals(enteredmobile) && dbOTP.equals("1")
						&& finalRoleId != enteredRoleId) {

					try {

						int count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=?  where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getUserSimSerialNo(),
										register.getKeyword(), register.getFirstName(), register.getLastName(),
										register.getFirmName(), register.getAddress(), register.getSchoolCode(),
										register.getEmailId(), register.getRoleId(), register.getPincode(),
										register.getPasscode(), register.getLatitude(), register.getLongitude(),
										register.getState(), register.getDistrict(), register.getTaluka(),
										register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

					} catch (Exception e) {
						// TODO: handle exception
					}
					return "OTPVerifiedAndRoleChanged";
				}

				/*******
				 * If mobile no is same and IMEI is different,update otp and
				 * send it to the user for verification
				 ********/

				if (!imei.equals(enteredImei) && uMobile.equals(enteredmobile) && finalRoleId == enteredRoleId) {
					Integer value = (int) (Math.random() * 9999) + 1001;
					otp = value.toString();
					int count = 0;
					int count1 = 0;
					try {

						count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,strDevId=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=?  where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getStrDevId(),
										register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
										register.getLastName(), register.getFirmName(), register.getAddress(),
										register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
										register.getPincode(), register.getPasscode(), register.getLatitude(),
										register.getLongitude(), register.getState(), register.getDistrict(),
										register.getTaluka(), register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

						count1 = jdbcTemplate.update("UPDATE StoreOTP set otp='" + otp + "' where appMobileNo=?",
								register.getAppMobileNo());

					} catch (Exception e) {
						// TODO: handle exception
					}
					messageTest.sendSms(register.getAppMobileNo(), OTPmsg, otp, space);
					return "DeviceChanged";
				}

				if (!imei.equals(enteredImei) && uMobile.equals(enteredmobile) && finalRoleId != enteredRoleId) {

					Integer value = (int) (Math.random() * 9999) + 1001;
					otp = value.toString();

					try {

						int count = jdbcTemplate.update(
								"UPDATE RegisterToMyCT set refMobileNo=?,strDevId=?,userSimSerialNo=?,keyword=?,firstName=?,lastName=?,"
										+ "firmName=?,address=?,schoolCode=?,emailId=?,roleId=?,pincode=?,passcode=?,latitude=?,longitude=?,state=?,"
										+ "district=?,taluka=?,userType=?,dealerMobileno=? from RegisterToMyCT where appMobileNo=?",
								new Object[] { register.getRefMobileNo(), register.getStrDevId(),
										register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
										register.getLastName(), register.getFirmName(), register.getAddress(),
										register.getSchoolCode(), register.getEmailId(), register.getRoleId(),
										register.getPincode(), register.getPasscode(), register.getLatitude(),
										register.getLongitude(), register.getState(), register.getDistrict(),
										register.getTaluka(), register.getUserType(), register.getDealerMobileno(),
										register.getAppMobileNo() });

						int count1 = jdbcTemplate.update("UPDATE StoreOTP set otp=? where appMobileNo=?", otp,
								register.getAppMobileNo());

					} catch (Exception e) {
						// TODO: handle exception
					}
					messageTest.sendSms(register.getAppMobileNo(), OTPmsg, otp, space);
					return "DeviceChangedWithRole";
				}

			}

		} else {

			try {
				int count = jdbcTemplate.update(
						"Insert into RegisterToMyCT(appMobileNo,refMobileNo,strDevId,userSimSerialNo,"
								+ "keyword,firstName,lastName,firmName,address,schoolCode,"
								+ "emailId,roleId,pincode,passcode,latitude,longitude,state,"
								+ "district,taluka,userType,dealerMobileno)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
						register.getAppMobileNo(), register.getRefMobileNo(), register.getStrDevId(),
						register.getUserSimSerialNo(), register.getKeyword(), register.getFirstName(),
						register.getLastName(), register.getFirmName(), register.getAddress(), register.getSchoolCode(),
						register.getEmailId(), register.getRoleId(), register.getPincode(), register.getPasscode(),
						register.getLatitude(), register.getLongitude(), register.getState(), register.getDistrict(),
						register.getTaluka(), register.getUserType(), register.getDealerMobileno());

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {

				SERVERID = jdbcTemplate.queryForObject("SELECT max(regId) from RegisterToMyCT", String.class);

			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
			}

			try {
				int count = jdbcTemplate.update("INSERT into StoreOTP (appMobileNo,otp)values(?,?)",
						register.getAppMobileNo(), otp);

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {

				int count = jdbcTemplate.update("INSERT into tbl_password(appMobileNo,userPassword,oauth)values(?,?,?)",
						register.getAppMobileNo(), password, oauth);

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {

				String sql = "Select regId,appMobileNo,refMobileNo,strDevId,userSimSerialNo,keyword,firstName,lastName,"
						+ "firmName,address,schoolCode,emailId,roleId,pincode,passcode,latitude,longitude,state,"
						+ "district,taluka,userType,dealerMobileno from RegisterToMyCT where appMobileNo=? && regId=?";

				registerToMyCT = jdbcTemplate.query(sql, new Object[] { register.getAppMobileNo(), SERVERID },
						new BeanPropertyRowMapper<RegisterToMyCT>(RegisterToMyCT.class));
			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
			}
			messageTest.sendSms(register.getAppMobileNo(), OTPmsg, otp, space);

		}
		return "Newregistration";
	}
	

	/************************************
	 * SUB USER REGISTER TO MY CITY
	 *******************************************/

	public int subUserRegister(SubUser subUser) {
		int count = 0;
		List<SubUser> subUserList = null;
		try {
			 count = jdbcTemplate.update(
					"Insert into subUser(user,password,address,companyName,emailId,firstName,phoneNumber,subuserName,subuserpassword)values(?,?,?,?,?,?,?,?,?)",
					subUser.getUser(),subUser.getPassword(),subUser.getAddress(),subUser.getCompanyName(),subUser.getEmailId(),subUser.getFirstName(),subUser.getPhoneNumber(),subUser.getSubuserName(),subUser.getSubuserpassword());

		} catch (Exception e) {
			// TODO: handle exception
		}

		return count;
	}
	
	/*********** Return registered data in response **********/

	public List<RegisterToMyCT> getRegisteredDataNew(RegisterToMyCT registerToMyCT) {

		List<RegisterToMyCT> list = null;

		try {

			String sql = "SELECT regId,appMobileNo,refMobileNo,strDevId,userSimSerialNo,keyword,firstName,"
					+ "lastName,firmName,address,schoolCode,emailId,roleId,pincode,passcode,"
					+ "latitude,longitude,state,district,taluka,userType,dealerMobileno from RegisterToMyCT "
					+ "where appMobileNo=? ";

			list = jdbcTemplate.query(sql, new Object[] { registerToMyCT.getAppMobileNo() },
					new BeanPropertyRowMapper<RegisterToMyCT>(RegisterToMyCT.class));

		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
		return list;
	}


}
