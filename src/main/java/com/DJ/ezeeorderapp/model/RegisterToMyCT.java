package com.DJ.ezeeorderapp.model;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName( "Registration" )

public class RegisterToMyCT implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String regId;
	private String appMobileNo;
	private String refMobileNo;
	private String strDevId;
	private String userSimSerialNo;
	private String keyword;
	private String firstName;
	private String lastName;
	private String firmName;
	private String address;
	private Integer schoolCode;
	private String emailId;
	private Integer roleId;
	private Integer pincode;
	private Integer passcode;
	private String latitude;
	private String longitude;
	private String state;
	private String district;
	private String taluka;
	private String userType;
	private String dealerMobileno;
	private String otp;
	private String userPassword;
	private String oauth;
	private String user;
	private String password;
	private String companyName;
	private String phoneNumber;
	private String subuserName;
	private String subuserpassword;
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getAppMobileNo() {
		return appMobileNo;
	}
	public void setAppMobileNo(String appMobileNo) {
		this.appMobileNo = appMobileNo;
	}
	public String getRefMobileNo() {
		return refMobileNo;
	}
	public void setRefMobileNo(String refMobileNo) {
		this.refMobileNo = refMobileNo;
	}
	public String getStrDevId() {
		return strDevId;
	}
	public void setStrDevId(String strDevId) {
		this.strDevId = strDevId;
	}
	public String getUserSimSerialNo() {
		return userSimSerialNo;
	}
	public void setUserSimSerialNo(String userSimSerialNo) {
		this.userSimSerialNo = userSimSerialNo;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirmName() {
		return firmName;
	}
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(Integer schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	public Integer getPasscode() {
		return passcode;
	}
	public void setPasscode(Integer passcode) {
		this.passcode = passcode;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getTaluka() {
		return taluka;
	}
	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getDealerMobileno() {
		return dealerMobileno;
	}
	public void setDealerMobileno(String dealerMobileno) {
		this.dealerMobileno = dealerMobileno;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getOauth() {
		return oauth;
	}
	public void setOauth(String oauth) {
		this.oauth = oauth;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSubuserName() {
		return subuserName;
	}
	public void setSubuserName(String subuserName) {
		this.subuserName = subuserName;
	}
	public String getSubuserpassword() {
		return subuserpassword;
	}
	public void setSubuserpassword(String subuserpassword) {
		this.subuserpassword = subuserpassword;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "RegisterToMyCT [regId=" + regId + ", appMobileNo=" + appMobileNo + ", refMobileNo=" + refMobileNo
				+ ", strDevId=" + strDevId + ", userSimSerialNo=" + userSimSerialNo + ", keyword=" + keyword
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", firmName=" + firmName + ", address="
				+ address + ", schoolCode=" + schoolCode + ", emailId=" + emailId + ", roleId=" + roleId + ", pincode="
				+ pincode + ", passcode=" + passcode + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", state=" + state + ", district=" + district + ", taluka=" + taluka + ", userType=" + userType
				+ ", dealerMobileno=" + dealerMobileno + ", otp=" + otp + ", userPassword=" + userPassword + ", oauth="
				+ oauth + ", user=" + user + ", password=" + password + ", companyName=" + companyName
				+ ", phoneNumber=" + phoneNumber + ", subuserName=" + subuserName + ", subuserpassword="
				+ subuserpassword + "]";
	}
	
}


