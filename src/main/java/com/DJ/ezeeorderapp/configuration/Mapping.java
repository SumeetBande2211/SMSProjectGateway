package com.DJ.ezeeorderapp.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

public class Mapping {
	public void getmapping(){
	ObjectMapper mapper = new ObjectMapper();
	mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true);
	mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
	mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
	
	mapper.setAnnotationIntrospector(new AnnotationIntrospectorPair(new  JacksonAnnotationIntrospector(), new JaxbAnnotationIntrospector(TypeFactory.defaultInstance())));
	
	
	}
}
