package com.DJ.ezeeorderapp.controller;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.DJ.ezeeorderapp.configuration.HelloWorldConfiguration;

public class AppMain {
	@SuppressWarnings({ "unused", "resource" })
	public static void main(String args[]) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(HelloWorldConfiguration.class);
	}
}
